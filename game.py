player = {"name": "Chris", "attack": 10, "heal": 16, "health": 100}
monster = {"name": "Brice", "attack": 12, "health": 100}
game_running = True

while game_running == True:

    print("Please select action.\n1) Attack\n2) Heal")
    player_choice = input()

    if player_choice == "1":
        print("Attack " + monster["name"])
        monster["health"] -= player["attack"]
        print(monster["name"] + " now has " + str(monster["health"]) + " health points.")
        if monster["health"] > 0:
            print(monster["name"] + " attacks back!")
            player["health"] -= monster["attack"]
            print(player["name"] + " now has " + str(player["health"]) + " health points.")
    elif player_choice == "2":
        print(player["name"] + " is healing!")
        player["health"] += player["heal"]
        print(player["name"] + " has " + str(player["health"]) + " health points")
    else:
        print("Invalid option!")

    if player["health"] <= 0 or monster["health"] <= 0:
        if player["health"] > 0 and monster["health"] <= 0:
            print(player["name"] + " has defeated " + monster["name"] + "!")
            game_running = False
        elif monster["health"] > 0 and player["health"] <= 0:
            print(monster["name"] + " has defeated " + player["name"] + "!")
            game_running = False

